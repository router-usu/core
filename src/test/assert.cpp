#include "assert.hpp"

static bool PassingBoolTest() {
	assert(true, "Simple boolean test failed");
	return true;
}

static bool FailingBoolTest() {
	assert(false, "Failing boolean test failed");
	return true;
}

void BoolTest(TestResults* results) {
    RunTest(PassingBoolTest, results);
    //RunTest(FailingBoolTest, results);
}
