#include "sgbuffer.hpp"
using namespace sg;

#if 0

Buffer::Buffer(std::shared_ptr<Segment> root) : m_root(root) {}


Segment::Segment(std::shared_ptr<uint8_t> buff, uint32_t len, uint32_t offs, bool attached)
	: m_buff(buff), m_length(len), m_offset(offs)
{}

std::shared_ptr<Segment> Segment::split(uint32_t offset) {
	if (offset < m_length) {
		return nullptr;
	}
	std::shared_ptr<Segment> part = std::make_shared<Segment>(m_buff, m_length - offset);
	m_length = offset;

	m_child = part;
	part->m_parent = std::shared_ptr<Segment>(this);
	part->m_attached = false;
}

void Segment::splice(std::shared_ptr<Segment> part) {
	m_child = part;
	part->m_parent = std::shared_ptr<Segment>(this);
	m_child->m_attached = true;
}

#endif
