// Opting for the old-school include gurad for sake of higher-level OSI headers unique include-gurad style
#ifndef ETH_FRAME_HPP
#define ETH_FRAME_HPP

#define ETH_MAXPAYLOAD 1500
#define ETH_PROT_ARP   0x0806
#define ETH_PROT_IPV4  0x0800

typedef struct {
	uint8_t addr[6];
} __attribute__((packed)) mac_t;

#include "arp_frame.hpp"
#include "ip4_frame.hpp"

//----------------------------------------------------------------------------+
// Ethernet 802.3/DIX frames                                                  |
typedef struct {
	mac_t dst_mac;
	mac_t src_mac;
	union {
		uint8_t eth_len[2];
		uint8_t eth_prot[2];
	};
} __attribute__((packed)) eth_header_t;

typedef struct {
	eth_header_t eth_header;
	union {
		uint8_t eth_payload[ETH_MAXPAYLOAD];
		arp_frame_t arp_frame;
		ip4_frame_t ip4_frame;
	};
} __attribute__((packed)) eth_frame_t;

#endif
