// A slightly untradional include guard but it ensures a few things:
//    1: Forward declarations work well for eth_frame.hpp
//    2: Relevenat macros and types about lower-level OSI frames are visible in higher-level headers
//    3: We acquire terse (easy to maintain) header files for each OSI level
//    4: Other sources don't inadvertenly include things in the wrong order
//    5: Every OSI level eventually gets included in eth_frame.hpp
#ifndef IP4_FRAME_HPP
#  error "icmp_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#ifdef  ICMP_FRAME_HPP
#  error "icmp_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#define ICMP_FRAME_HPP

//----------------------------------------------------------------------------+
// ICMP Header                                                                |
typedef struct {
	uint8_t type;
	uint8_t code;
	uint8_t crc[2];
	union
	{
		uint8_t header[4];
		struct
		{
			uint8_t ident[2];
			uint8_t seqno[2];
		} echo;
	};
} __attribute__((packed)) icmp_header_t;

#define ICMP_MAXPAYLOAD IP4_MAXPAYLOAD - sizeof(icmp_header_t)

//----------------------------------------------------------------------------+
// ICMP Packet Layout                                                         |
typedef struct {
	icmp_header_t icmp_header;
	uint8_t data[ICMP_MAXPAYLOAD];
} __attribute__((packed)) icmp_frame_t;
