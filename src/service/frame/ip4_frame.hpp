// A slightly untradional include guard but it ensures a few things:
//    1: Forward declarations work well for eth_frame.hpp
//    2: Relevenat macros and types about lower-level OSI frames are visible in higher-level headers
//    3: We acquire terse (easy to maintain) header files for each OSI level
//    4: Other sources don't inadvertenly include things in the wrong order
//    5: Every OSI level eventually gets included in eth_frame.hpp
#ifndef ETH_FRAME_HPP
#  error "ip4_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#ifdef  IP4_FRAME_HPP
#  error "ip4_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#define IP4_FRAME_HPP

//----------------------------------------------------------------------------+
// IPv4 Header                                                                |
typedef struct {
	uint8_t ver_ihl;
	uint8_t dscp;

	uint8_t length[2];
	uint8_t ident[2];

	uint8_t frag[2];

	uint8_t ttl;
	uint8_t prot;
	uint8_t crc[2];
	uint8_t src[4];
	uint8_t dst[4];
} __attribute__((packed)) ip4_header_t;

//----------------------------------------------------------------------------+
// IPv4 Payload Size                                                          |
#define IP4_MAXPAYLOAD ETH_MAXPAYLOAD - sizeof(ip4_header_t)

//----------------------------------------------------------------------------+
// IPv4 Payload Types                                                         |
#include "icmp_frame.hpp"

//----------------------------------------------------------------------------+
// IPv4 Packet Layout                                                         |
typedef struct {
	ip4_header_t ip4_header;
	union {
		uint8_t ip4_payload[IP4_MAXPAYLOAD];
		icmp_frame_t icmp_frame;
	};
} __attribute__((packed)) ip4_frame_t;
