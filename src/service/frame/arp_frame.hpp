// A slightly untradional include guard but it ensures a few things:
//    1: Forward declarations work well for eth_frame.hpp
//    2: Relevenat macros and types about lower-level OSI frames are visible in higher-level headers
//    3: We acquire terse (easy to maintain) header files for each OSI level
//    4: Other sources don't inadvertenly include things in the wrong order
//    5: Every OSI level eventually gets included in eth_frame.hpp
#ifndef ETH_FRAME_HPP
#  error "arp_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#ifdef  ARP_FRAME_HPP
#  error "arp_frame.hpp should not be directly included; include eth_frame.hpp instead"
#endif
#define ARP_FRAME_HPP

typedef struct {
	mac_t mac;
	uint8_t ip[4];
} __attribute__((packed)) arp_entry_t;

//----------------------------------------------------------------------------+
// ARP Header                                                                 |
typedef struct {
	uint8_t hwtype[2];
	uint8_t prottype[2];
	uint8_t hwlength;
	uint8_t protlength;
	uint8_t opcode[2];
} __attribute__((packed)) arp_header_t;

//----------------------------------------------------------------------------+
// ARP Payload Size                                                           |
#define ARP_MAXPAYLOAD ETH_MAXPAYLOAD - sizeof(arp_header_t)

//----------------------------------------------------------------------------+
// ARP Packet Layout                                                          |
typedef struct {
	arp_header_t arp_header;
	union {
		uint8_t arp_payload[ARP_MAXPAYLOAD];
		struct {
			arp_entry_t src_arp;
			arp_entry_t dst_arp;
		};
	};
} __attribute__((packed)) arp_frame_t;
