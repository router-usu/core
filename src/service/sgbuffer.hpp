#pragma once
#include <functional>
#include <memory>
#include <stdint.h>
#include <vector>
#include <stdexcept>

/*
 * @namespace Scatter Gather
 * Contains classes and functions for smart management of buffers that can be either contiguous or fragmented
 */
namespace sg {

/*
 * @class Segment
 * Accesses a portion of a raw byte buffer
 * This class is the external interface for interacting with the data
 * Other classes in the namespace manage this object in a smart way
 */
class Segment {
private:
	std::shared_ptr<uint8_t> m_buff;
	uint8_t* m_raw;
	uint16_t m_offset;
	uint16_t m_length;

	inline       void     protect(uint16_t i)    const { if (i >= m_length) { throw std::out_of_range("sg::Segment index oob"); } }
public:
	inline       uint16_t length()               const { return m_length; }
	inline       uint8_t& operator[](uint16_t i)       { protect(i); return m_raw[i]; }
	inline const uint8_t& operator[](uint16_t i) const { protect(i); return m_raw[i]; }

	inline uint8_t* get() { return m_raw; }

	/*
	 * @brief Helper for convert casting primarily to a struct type
	 * Casting raw buffers as structs is a common and useful practice, but it does require somewhat exposing a raw pointer.
	 * Special care must be taken that the return value of this function does not persist beyond the lifetime of the Segment.
	 * Also, don't attempt to free the pointer returned...that would be silly.
	 */
	template <typename T>
	inline T* as() { return (T*)m_raw; }

	template <typename T>
	inline void as(std::function<void(      T*, uint16_t)> callback)       { callback((T*)m_raw, m_length); }
	template <typename T>
	inline void as(std::function<void(const T*, uint16_t)> callback) const { callback((T*)m_raw, m_length); }

	inline       Segment split_head(uint16_t offset)       { return Segment(m_buff, 0, offset); }
	inline const Segment split_head(uint16_t offset) const { return Segment(m_buff, 0, offset); }

	inline       Segment split_tail(uint16_t offset)       { return Segment(m_buff, offset, m_length - offset); }
	inline const Segment split_tail(uint16_t offset) const { return Segment(m_buff, offset, m_length - offset); }

	Segment(std::shared_ptr<uint8_t> buff, uint16_t offset, uint16_t len)
		: m_buff(buff), m_offset(offset), m_length(len), m_raw(buff.get() + offset) {}

	Segment() = default;
	Segment(const Segment&) = default;
	Segment& operator=(const Segment &) = default;
	~Segment() = default;
};

// A thought anyways
template <typename T>
class SegmentOf {
private:
	std::shared_ptr<Segment> m_seg;
	T* m_raw;

};

/*
	template <typename T>
	std::shared_ptr<T> gather();

	std::shared_ptr<Segment> split(uint32_t offset);
	void splice(std::shared_ptr<Segment> part);
*/

/*
 *
 */
class Chunk {
	std::shared_ptr<uint8_t> m_buff;
	uint16_t m_size;
	uint16_t m_front;
	uint16_t m_back;

};

class Buffer {
private:
	struct Stitch {
		Segment seg;
		uint16_t head;
		uint16_t tail;
	};
	std::vector<std::shared_ptr<Segment>> m_segments;

public:
	Buffer(std::shared_ptr<Segment> root);
/*
	template <typename T>
	std::shared_ptr<T> gather() {
		return m_root->gather<T>();
	}
*/
};

/*
template <typename T>
std::shared_ptr<T> Segment::gather() {
	return nullptr;
}
*/

};
