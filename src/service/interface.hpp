#pragma once
#include <stdint.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include <memory>

class Interface {
private:
	std::string name;
	uint8_t mac[6];
	int handle;
	
	Interface() {}

public:
	static std::shared_ptr<Interface> getInterface(std::string device);
	~Interface() { close(handle); }

	// Prevent default class operations on this class
	Interface& operator=(const Interface&) = delete;
	Interface(const Interface&) = delete;

	inline const uint8_t* address() const { return mac; }

	inline int send(uint8_t* frame, int len) const {
		return write(handle, frame, len);
	}
	inline int receive(uint8_t* buff, int len) const {
		return recvfrom(handle, buff, len, 0, nullptr, nullptr);
	}
};
