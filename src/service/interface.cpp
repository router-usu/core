#include "interface.hpp"
#include <sys/ioctl.h>
#include <net/if.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <string.h>
#include <netinet/in.h>

std::shared_ptr<Interface> Interface::getInterface(std::string device) {
	std::shared_ptr<Interface> inf = nullptr;
	struct sockaddr_ll sll = { 0 };
	struct ifreq ifreq;

	sll.sll_family = AF_PACKET;
	sll.sll_protocol = htons(ETH_P_ALL);

	int handle = socket(sll.sll_family, SOCK_RAW, sll.sll_protocol);
	if (handle < 0) return nullptr;

	// Enable writing to the interface
	strcpy(ifreq.ifr_name, device.c_str());
	ioctl(handle, SIOCGIFINDEX, &ifreq);
	sll.sll_ifindex = ifreq.ifr_ifindex;

	bind(handle, (struct sockaddr*)&sll, sizeof(sockaddr_ll));

	// get the mac address
	struct ifreq ifr;
	strncpy(&ifr.ifr_name[0], device.c_str(), IFNAMSIZ);
	if (ioctl(handle, SIOCGIFHWADDR, &ifr) >= 0) 
	{
		inf = std::shared_ptr<Interface>(new Interface());
		inf->name = device;
		inf->handle = handle;
		std::memcpy(inf->mac, &ifr.ifr_hwaddr.sa_data, 6);
	}

	return inf;
}
