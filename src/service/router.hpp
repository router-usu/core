#pragma once
#include "nat.hpp"
#include <memory>
#include <map>

/*
 * Class Router
 * Singleton used for accessing all of the data structures required to operate the router
 */
class Router {
private:
	Router();

	static const std::shared_ptr<Router> get();
	static std::shared_ptr<Router> m_self;

	std::shared_ptr<NatList> m_nat;
	std::map<std::string, std::shared_ptr<Interface>> m_interfaces;

public:
	static inline const std::shared_ptr<NatList> nat() { return get()->m_nat; }
	static inline const std::shared_ptr<Interface> inteface(std::string dev) {
		auto self = get();
		auto& iList = self->m_interfaces;
		if (iList.find(dev) == iList.end()) {
			return nullptr;
		}
		return iList[dev];
	}

	~Router() {}
	Router(const Router&) = delete;
	Router& operator=(const Router& rhs) = delete;
};
