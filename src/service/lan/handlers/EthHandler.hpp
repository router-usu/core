#pragma once
#include <cstring>
#include <stdint.h>
#include <vector>
#include "service/sgbuffer.hpp"
#include "service/frame/eth_frame.hpp"

typedef uint32_t ph_error_t;

#define     ERROR_NONE                      0
#define     ERROR_MALFORMED_PACKET          1
#  define   ERROR_ETH_MALFORMED_PACKET      (1 << 8) & ERROR_MALFORMED_PACKET
#    define ERROR_ETH_TOO_SMALL             (1 << 8) & ERROR_ETH_MALFORMED_PACKET
#    define ERROR_ETH_UNRECOGNIZED_PROTOCOL (2 << 8) & ERROR_ETH_MALFORMED_PACKET

#define is(err, check)

class EthHandler {
public:
	static ph_error_t handle(sg::Segment&);

protected:
	sg::Segment m_eth_frame;
	inline void eth_frame(std::function<void(      eth_frame_t*, uint16_t)> callback)       { m_eth_frame.as<eth_frame_t>(callback); }
	inline void eth_frame(std::function<void(const eth_frame_t*, uint16_t)> callback) const { m_eth_frame.as<eth_frame_t>(callback); }

protected:
	// As much as I would love to simply typecast, that would only work on BE systems
	static inline uint16_t parse16(uint8_t* buff) {
		return (buff[0] << 8 | buff[1] << 0);
	}
	static inline uint32_t parse32(uint8_t* buff) {
		return (buff[0] << 24 | buff[1] << 16 | buff[2] << 8 | buff[3] << 0);
	}
	static inline void write16(uint8_t* buff, uint16_t val) {
		buff[0] = (val & 0xFF00) >> 8;
		buff[1] = (val & 0x00FF) >> 0;
	}
	static inline void write32(uint8_t* buff, uint32_t val) {
		buff[0] = (val & 0xFF000000) >> 24;
		buff[1] = (val & 0x00FF0000) >> 16;
		buff[2] = (val & 0x0000FF00) >>  8;
		buff[3] = (val & 0x000000FF) >>  0;
	}

	// Prevent copy operations, they make no sense here
	EthHandler(sg::Segment eth) : m_eth_frame(eth) {}
	EthHandler(const EthHandler& rhs) = delete;
	EthHandler& operator=(const EthHandler& rhs) = delete;
};
