#pragma once
#include "EthHandler.hpp"

class Ip4Handler : public EthHandler {
public:
	static ph_error_t handle(sg::Segment&);

private:
	//static std::unordered_map<int, sg::Segment> arp_cache;
	Ip4Handler(const Ip4Handler& rhs) = delete;
	Ip4Handler& operator=(const Ip4Handler& rhs) = delete;
};
