#include "EthHandler.hpp"
#include "ArpHandler.hpp"
#include "Ip4Handler.hpp"

ph_error_t EthHandler::handle(sg::Segment& eth) {
	eth_header_t* eth_header = eth.as<eth_header_t>();

	uint16_t len = eth.length();
	if (len < sizeof(eth_header_t)) {
		return ERROR_ETH_TOO_SMALL;
	}

	sg::Segment payload = eth.split_tail(offsetof(eth_frame_t, eth_payload));

	uint16_t prot = parse16(eth_header->eth_prot);
	switch (prot) {
	case ETH_PROT_ARP:
		return ArpHandler::handle(payload);
	case ETH_PROT_IPV4:
		return Ip4Handler::handle(payload);
	default:
		return ERROR_ETH_UNRECOGNIZED_PROTOCOL;
	}
}
