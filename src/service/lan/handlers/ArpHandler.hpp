#pragma once
#include "EthHandler.hpp"

class ArpHandler : public EthHandler {
public:
	static ph_error_t handle(sg::Segment&);

private:
	//static std::unordered_map<int, sg::Segment> arp_cache;
	ArpHandler(const ArpHandler& rhs) = delete;
	ArpHandler& operator=(const ArpHandler& rhs) = delete;
};
