#include "Ip4Handler.hpp"

ph_error_t Ip4Handler::handle(sg::Segment& eth) {
	eth_header_t* eth_header = eth.as<eth_header_t>();

	uint16_t len = eth.length();
	if (len < sizeof(eth_header_t)) {
		return ERROR_ETH_TOO_SMALL;
	}

	sg::Segment payload = eth.split_tail(offsetof(eth_frame_t, eth_payload));

}
