#include "router.hpp"

// lazy loaded singleton reference
std::shared_ptr<Router> Router::m_self;
const std::shared_ptr<Router> Router::get() {
	if (m_self == nullptr) {
		m_self = std::shared_ptr<Router>(new Router());
	}
	return m_self;
}

Router::Router() {
	m_nat = std::make_shared<NatList>();

	// Setup the interfaces
	std::string iList[] = { "eth0", "eth1" };
	int interfaces = sizeof(iList) / sizeof(std::string);
	for (int i = 0; i < interfaces; ++i) {
		std::string& device = iList[i];
		m_interfaces[device] =  Interface::getInterface(device);
	}
}
