#pragma once
#include "fifo.hpp"
#include <memory>
#include <thread>

/*
 * Class Threadpool
 * Singleton used for spawning threads in a controlled way to ensure we join on all of them
 */
class Threadpool {
private:
	Threadpool() = default;

	static const std::shared_ptr<Threadpool> get();
	static std::shared_ptr<Threadpool> m_self;

	Fifo<std::shared_ptr<std::thread>> m_fifo;

public:
	static void join();
	static void manage(std::shared_ptr<std::thread> pThread);

	~Threadpool() = default;
	Threadpool(const Threadpool&) = delete;
	Threadpool& operator=(const Threadpool& rhs) = delete;
};
