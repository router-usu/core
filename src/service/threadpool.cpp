#include "threadpool.hpp"

// Lazy loaded singletone
std::shared_ptr<Threadpool> Threadpool::m_self;
const std::shared_ptr<Threadpool> Threadpool::get() {
	if (m_self == nullptr) {
		m_self = std::shared_ptr<Threadpool>(new Threadpool());
	}
	return m_self;
}

void Threadpool::join() {
	auto self = get();
	auto& fifo = self->m_fifo;
	while (!fifo.empty()) {
		std::shared_ptr<std::thread> pThread;
		fifo.popItem(pThread);
		pThread->join();
	}
}

void Threadpool::manage(std::shared_ptr<std::thread> pThread) {
	auto self = get();
	auto& fifo = self->m_fifo;
	fifo.pushItem(pThread);
}
