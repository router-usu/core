#include "service/router.hpp"
#include "service/threadpool.hpp"
#include "service/frame/eth_frame.hpp"
#include "service/lan/handlers/EthHandler.hpp"
#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("Hello World\n");

	Fifo<sg::Segment> lanRecv;

	auto eth0 = Router::inteface("eth0");

	if (eth0 == nullptr) {
		printf("Device not found: etho0\n");
		return 1;
	}

	Threadpool::manage(std::make_shared<std::thread>([&lanRecv] {
		sg::Segment packet;
		lanRecv.popItem(packet);
		while (packet.length() > 0) {
			EthHandler::handle(packet);
			lanRecv.popItem(packet);
		}
	}));

	Threadpool::manage(std::make_shared<std::thread>([&eth0, &lanRecv] {
		std::shared_ptr<uint8_t> buff((uint8_t*)malloc(sizeof(eth_frame_t)), free);
		while (1) {
			int n = eth0->receive(buff.get(), sizeof(eth_frame_t));
			printf("Received %i bytes\n", n);
			if (n <= 0) {
				lanRecv.pushItem(sg::Segment(nullptr, 0, 0));
				return;
			}
			lanRecv.pushItem(sg::Segment(buff, 0, n));
		}
	}));

	Threadpool::join();
	return 0;
}
