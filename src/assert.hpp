#pragma once
#include <stdio.h>
#include <stdbool.h>

#define assert(test, msg)                                                       \
	if (!(test)) {                                                              \
		printf("Assert failed in: %s:%d\n\t%s\n", __FILE__, __LINE__, msg);     \
		return false;                                                           \
	}

typedef bool (*UnitTest)();

typedef struct {
	int total;
	int passed;
	int failed;
} TestResults;

static inline void RunTest(UnitTest func, TestResults* results) {
	++(results->total);
	if (!func()) {
		++(results->failed);
	}
	else {
		++(results->passed);
	}
}
