#include "assert.hpp"

typedef void (*UnitTestGroup)(TestResults*);

typedef struct {
	UnitTestGroup func;
	const char* name;
} TestProcedure;

extern void BoolTest(TestResults*);

static TestProcedure Tests[] = {
	{ BoolTest, "Verification of Unit Test Framework" },
};

int main() {
	// A tally of higher-level test groups that pass/fail
	TestResults results = { 0 };
	results.total = sizeof(Tests) / sizeof(TestProcedure);
	
	// A tally of lower-level units that pass/fail
	TestResults uresults = { 0 };
	
	printf(
		"+------------------------------------------------+\n"
		"|           Router USU Unit Test Suite           |\n"
		"+------------------------------------------------+\n\n"
	);
	for (int tid = 0; tid < results.total; ++tid) {
		TestProcedure* test = Tests + tid;
		TestResults tresults = { 0 };
		
		printf(
			"--------------------------------------------------\n"
			"Executing Test Group \"%s\":\n",
			test->name
		);

		test->func(&tresults);
		
		printf(
			"+--------------------+\n"
			"|    Test Summary    |\n"
			"+--------------------+\n"
			"|    Execed: %04d    |\n"
			"|    Passed: %04d    |\n"
			"|    Failed: %04d    |\n"
			"+--------------------+\n",
			tresults.total,
			tresults.passed,
			tresults.failed
		);
		
		if (!(tresults.failed)) {
			++(results.passed);
		}
		else {
			++(results.failed);
		}
		
		uresults.total += tresults.total;
		uresults.passed += tresults.passed;
		uresults.failed += tresults.failed;
		
		printf(
			"--------------------------------------------------\n\n"
		);
	}
	
	printf(
		"+---------------------------+\n"
		"|      Abridged Summary     |\n"
		"+---------------------------+\n"
		"|           | Tests | Units |\n"
		"|    Execed | %05d | %05d |\n"
		"|    Passed | %05d | %05d |\n"
		"|    Failed | %05d | %05d |\n"
		"+---------------------------+\n"
		"%s"
		"+---------------------------+\n",
		results.total, uresults.total,
		results.passed, uresults.passed,
		results.failed, uresults.failed,
		
		results.failed ?
		"|           FAILED          |\n" :
		"|           PASSED          |\n"
	);
	
	return uresults.failed;
}
