#Project Tree Structure
LIBRARY_DIR    := lib
DEPEND_DIR     := dep
OBJECT_DIR     := obj
BINARY_DIR     := bin

SOURCE_DIR     := src
SERVICE_DIR    := $(SOURCE_DIR)/service
UNITEST_DIR    := $(SOURCE_DIR)/test

#Compiler flag variables
INC            := $(SOURCE_DIR)
INC_PARAMS     := $(foreach d, $(INC), -I$d)
DEPFLAGS        = -MT $(1).o -MMD -MP -MF $(DEPEND_DIR)/$(1).Td
CFLAGS         := -std=c++14 -g $(INC_PARAMS)
LINKFLAGS      := -pthread

#Cross Comipler Constants
NATIVE_CPPC    := g++
CROSS_CPPC_DIR := gcc-linaro-6.4.1-2017.11-x86_64_arm-linux-gnueabihf/bin
CROSS_CPPC     := $(CROSS_CPPC_DIR)/arm-linux-gnueabihf-g++
ARCH_CPPC      := $(NATIVE_CPPC)

#The full command for compilation
CPPC            = $(ARCH_CPPC) $(CPPFLAGS) $(INC_PARAMS) $(DEPFLAGS)
CPPL           := $(ARCH_CPPC) $(CPPFLAGS)
POSTCPPC        = @mv -f $(DEPEND_DIR)/$(1).Td $(DEPEND_DIR)/$(1).d && touch $(OBJECT_DIR)/$(1).o

#Automatically determine the project structure
STRUCTURE      := $(patsubst $(SOURCE_DIR)/%,%,$(shell find $(SOURCE_DIR)/* -type d))
OBJECT_DIRS    := $(OBJECT_DIR) $(patsubst %,$(OBJECT_DIR)/%,$(STRUCTURE))
DEPEND_DIRS    := $(DEPEND_DIR) $(patsubst %,$(DEPEND_DIR)/%,$(STRUCTURE))
DIRS           := $(DEPEND_DIRS) $(OBJECT_DIRS) $(BINARY_DIR)

#Enumerate all source files for dependency management
SERVICE_SRCS   := $(patsubst $(SOURCE_DIR)/%,%,$(shell find $(SERVICE_DIR) -name '*.cpp'))
UNITEST_SRCS   := $(patsubst $(SOURCE_DIR)/%,%,$(shell find $(UNITEST_DIR) -name '*.cpp'))
#SRCS           := service.cpp $(SERVICE_SRCS) test.cpp $(UNITEST_SRCS)

#Enumerate the object file dependencies for each binary
SERVICE_OBJ    := $(patsubst %.cpp,$(OBJECT_DIR)/%.o,$(SERVICE_SRCS))
UNITEST_OBJ    := $(patsubst %.cpp,$(OBJECT_DIR)/%.o,$(UNITEST_SRCS))
SERVICE_DEPS   := $(OBJECT_DIR)/service.o $(SERVICE_OBJ)
UNITEST_DEPS   := $(OBJECT_DIR)/test.o    $(SERVICE_OBJ) $(UNITEST_OBJ)
TARGET_OBJS    := $(OBJECT_DIR)/service.o $(SERVICE_OBJ) $(UNITEST_OBJ) $(OBJECT_DIR)/test.o
TARGET_BASE    := $(patsubst $(OBJECT_DIR)/%.o,%,$(TARGET_OBJS))

#Default make: get the toolchain, build the service, build and run unit tests
.PHONY: all clean toolchain service test smain tmain
all: | toolchain service test

clean:
	rm -rf $(DEPEND_DIR) $(OBJECT_DIR) $(BINARY_DIR)

#Link step recipes
$(BINARY_DIR)/service: $(SERVICE_DEPS)
	$(CPPL) -o $@ $^ $(LINKFLAGS)
$(BINARY_DIR)/test: $(UNITEST_DEPS)
	$(CPPL) -o $@ $^ $(LINKFLAGS)

#Compile step recipes with dependency generation
define COMPILE_OBJ =
$(OBJECT_DIR)/$(1).o: $(SOURCE_DIR)/$(1).cpp $(DEPEND_DIR)/$(1).d
	$(CPPC) -c -o $(OBJECT_DIR)/$(1).o $(SOURCE_DIR)/$(1).cpp
	$(POSTCPPC)

#Dependency management
$(DEPEND_DIR)/$(1).d: ;
.PRECIOUS: $(DEPEND_DIR)/$(1).d

-include $(DEPEND_DIR)/$(1).d
endef
#Careful not to put a space after the last ',' in call
$(foreach unit, $(TARGET_BASE), $(eval $(call COMPILE_OBJ,$(unit))))

#$(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(DEPEND_DIR)/%.d
#	$(CPPC) -c -o $@ $<
#	$(POSTCPPC)

#Ensure that the toolchain is ready for any given build
smain: $(BINARY_DIR)/service
service: | toolchain smain

tmain: $(BINARY_DIR)/test
test: | toolchain tmain
	@$(BINARY_DIR)/test

#Toolchain includes downloading and building any cross-platform compilers
#And setting up a mirrored structure in the object and dependency folders
toolchain: | $(CROSS_CPPC_DIR) $(DIRS)
#	@echo $(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(DEPEND_DIR)/%.d
#	@echo $(TARGET_BASE)

#Structure dependencies
define SUBDIR =
$(1):
	@mkdir -p $(1)
endef
$(foreach dir, $(DIRS), $(eval $(call SUBDIR, $(dir))))

#Cross compiler target downloads the arm cross-compiler if it is not present on the system
$(CROSS_CPPC_DIR):
	wget -c https://releases.linaro.org/components/toolchain/binaries/6.4-2017.11/arm-linux-gnueabihf/gcc-linaro-6.4.1-2017.11-x86_64_arm-linux-gnueabihf.tar.xz
	tar xf gcc-linaro-6.4.1-2017.11-x86_64_arm-linux-gnueabihf.tar.xz
	rm gcc-linaro-6.4.1-2017.11-x86_64_arm-linux-gnueabihf.tar.xz

#Pattern notation/wildcard does not appear to work for nested directories
#Dependency management
#$(DEPEND_DIR)/%.d: ;
#.PRECIOUS: $(DEPEND_DIR)/%.d

#include $(wildcard $(patsubst %,$(DEPEND_DIR)/%.d,$(basename $(SRCS))))
